'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    mainBowerFiles = require('main-bower-files'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    babel = require('gulp-babel'),
    browserSync = require("browser-sync"),
    notify = require('gulp-notify'),
    reload = browserSync.reload;
    // iconfont = require('gulp-iconfont'),
    // iconfontCss = require('gulp-iconfont-css'),
    // fontName = 'Trubman-icons';

var path = {
    vendor: {
        js: 'app/js/vendor/',
        css: 'app/css/vendor/'
    },
    public: { 
        html: 'public/',
        js: 'public/js/',
        scss: 'public/css/',
        css: 'public/css/',
        img: 'public/img/',
        fonts: 'public/fonts/',
        template: 'public/template/'
    },
    app: { 
        html: 'app/*.html',
        js: 'app/js/*.js',
        scss: 'app/css/*.scss',
        css: 'app/css/*.css',
        scssIconFont: 'app/css/_icons.scss',
        img: 'app/img/**/*.*',
        icons: 'app/img/trumpet/*.svg',
        fonts: 'app/fonts/**/*.*',
        fontsIcons: 'app/fonts/trumpet/',
        template: 'app/template/*.html',
        templateIconFont: 'app/css/template/_icons_template.scss',
    },
    watch: {
        html: 'app/**/*.html',
        js: 'app/js/**/*.js',
        scss: 'app/css/**/*.scss',
        css: 'app/css/**/*.css',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        template: 'app/template/*.html'
    },
    clean: './public'
};
var config = {
    server: {
        baseDir: "./public"
    },
    tunnel: false,
    host: 'localhost',
    port: 3000,
    logPrefix: "Crius.Agency"
};
gulp.task('iconfont', function () {
    gulp.src(path.app.icons)
        .pipe(iconfontCss({
            path: path.app.templateIconFont,
            fontName: fontName,
            targetPath: '../../css/_other-icon.scss',
            fontPath: path.app.fontsIcons,
            normalize: true,
            fontHeight: 1001
        }))
        .pipe(iconfont({
            fontName: fontName,
            formats: ['woff2', 'svg']
        }))
        .pipe(gulp.dest('app/fonts/trumpet'));
});
// gulp.task('move', function () {
//   return gulp
//     .src(path.app.models)
//     .pipe(gulp.dest(path.public.models));
// });

gulp.task('vendorJs:build', function () {
    gulp.src( mainBowerFiles('**/*.js'))
        .pipe(gulp.dest(path.vendor.js))
        .pipe(gulp.dest(path.public.js))
});

gulp.task('vendorCss:build', function () {
    gulp.src( mainBowerFiles('**/*.css'))
        .pipe(gulp.dest(path.vendor.css))
        .pipe(gulp.dest(path.public.css))
});

gulp.task('html:build', function () {
    gulp.src(path.app.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.public.html))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('js:build', function () {
    gulp.src(path.app.js)
        // .pipe(rigger())
        // .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify().on('error', notify.onError({
            message: "<%= error.message %>",
            title: "JS Error 😡!"
        })))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.js))
        .pipe(reload({
            stream: true
        }));
        // .pipe(notify({
        //     message: "Все хорошо!",
        //     title: "JS Good 😁!"
        // }));
});

gulp.task('sass:build', function () {
    gulp.src(path.app.scss)
        // .pipe(sourcemaps.init())
        .pipe(sass().on('error', notify.onError({
                message: "<%= error.message %>",
            title: "Sass Error 😡!"
        })))
        .pipe(prefixer()) 
        .pipe(cssmin())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.css))
        .pipe(reload({
            stream: true
        }));
        // .pipe(notify({
        //     message: "Все хорошо",
        //     title: "Sass Good 😁!"
        // }));
});

gulp.task('css:build', function () {
    gulp.src(path.app.css)
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.app.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.public.img))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('fonts:build', function() {
    gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.public.fonts))
});

gulp.task('build', [
    'vendorCss:build',
    'vendorJs:build',
    'html:build',
    'js:build',
    'sass:build',
    'css:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.template], function (event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.scss], {readDelay: 500}, function(event, cb) {
        gulp.start('sass:build');
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});



// gulp.task('default', ['build', 'webserver', 'watch', 'move']);
gulp.task('default', ['build', 'webserver', 'watch']);
